	<?php include 'header.php'; ?>
<!-- 					<section>
<iframe id="map_wrap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9932.023512732714!2d6.937827676083791!3d50.933772302052134!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa2fc5d7248a2f37c!2sengine-productions+GmbH!5e0!3m2!1sde!2sde!4v1458567121645"  height="512" frameborder="0" style="border:0" allowfullscreen></iframe>
					</section> -->
					<section>
    					<div id="map_wrap">
    						<div id="map"></div>
    					</div>
    				</section>
    				<section>
    					<div class="text">
	    					<div class="fifty">
								<h2>engine-productions GmbH</h2>
								<h3>Büro für interaktive Medien</h3>
								<p><b>Geschäftsführung:</b><br>Dipl.-Ing. (FH) Christian Spatz<br> Dipl.-Ing. (FH) Jörn Gahrmann </p>
								<p><b>Firmensitz:</b><br>Lindenstraße 20<br> 50674 Köln </p>
								<p><b>Tel.: </b> +49 (0)2 21 / 56 95 91 - 80 <br> <b>Fax:</b> +49 (0)2 21 / 56 95 91 - 81 </p>
								<p><b>Mail:</b> <a href="mailto:info@engine-productions.de">info@engine-productions.de</a><br> <b>Web:</b> <a href="http://www.engine-productions.de//" target="_blank" external="1">http://www.engine-productions.de</a></p>
								<p><a href="http://www.engine-productions.de//" target="_blank" external="1"></a> </p>
								<p><b>HRB:</b>  68827<br> <b>Amtsgericht:</b> Köln </p>
								<p><b>USt.-ID:</b> DE235435173  </p>
								<p><br/>engine-productions® ist eine eingetragene Marke der engine-productions GmbH Copyright © 2004-2016 engine-productions GmbH </p>
	    					</div>
	    					<div class="fifty">
								<p><a target="_blank" href="https://www.facebook.com/engineprod">Facebook</a></p>
								<p><a target="_blank" href="https://twitter.com/engineprod">Twitter</a></p>
								<p><a target="_blank" href="http://www.koelndesign.de/de/dienstleister/profile/engine-productions-medienproduktion">KölnDesign</a></p>
								<p><a target="_blank" href="https://www.xing.com/companies/engine-productionsgmbh-b%C3%BCrof%C3%BCrinteraktivemedien">Xing</a></p>
	    					</div>
	    					<div class="clear"></div>
    					</div>
 						<script src="https://maps.googleapis.com/maps/api/js" async defer></script>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>

	<?php include 'footer.php'; ?>