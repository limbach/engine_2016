	<?php include 'header.php'; ?>

					<section>
						<div class="image">
							<img src="img/slide2.jpg" />
						</div>
					</section>

					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>

	<?php include 'footer.php'; ?>