	<?php include 'header.php'; ?>

					<section>
						<div class="grid">
							<div class="grid-sizer"></div>
							<div class="grid-item zwei_eins" style="background:url('img/projekte/tw.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo"><img src="img/logos/bmel.svg" /></div>
								<div class="grid-item-image"><img src="img/projekte/tw.jpg" /></div>
								<div class="grid-item-text">Tierwohl</div>
							</div>
							<div class="grid-item zwei_zwei" style="background:url('img/projekte/tw.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo"><img src="img/logos/bmel.svg" /></div>
								<div class="grid-item-image"><img src="img/projekte/md.jpg" /></div>
								<div class="grid-item-text">Macht Dampf für gutes Essen in Kita und Schule</div>
							</div>
							<div class="grid-item zwei_eins" style="background:url('img/projekte/tw.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo"><img src="img/logos/bmfsfj.svg" /></div>
								<div class="grid-item-image"><img src="img/projekte/mgh.jpg" /></div>
								<div class="grid-item-text">mgh</div>
							</div>
							<div class="grid-item zwei_eins" style="background:url('img/projekte/tw.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo">
									<img src="img/logos/vdk.svg" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/barriere.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Warum ist Barrierefreiheit wichtig
								</div>
							</div>
							<div class="grid-item zeins_zwei" style="background:url('img/projekte/tw.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo">
									<img src="img/logos/smartfrog.png" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/sf.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Smartfrog
								</div>
							</div>
							<div class="grid-item zwei_eins" style="background:url('img/projekte/tw.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo">
									<img src="img/logos/bmel.svg" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/tw.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Tierwohl
								</div>
							</div>
							<div class="grid-item eins_zwei" style="background:url('img/projekte/sf.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo">
									<img src="img/logos/smartfrog.png" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/sf.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Smartfrog
								</div>
							</div>
							<div class="grid-item zwei_zwei" style="background:url('img/projekte/md.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo">
									<img src="img/logos/bmel.svg" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/md.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Macht Dampf für gutes Essen in Kita und Schule
								</div>
							</div>

							<div class="grid-item zwei_zwei" style="background:url('img/projekte/md.jpg') no-repeat top left;background-size:cover;">
								<div class="grid-item-logo">
									<img src="img/logos/bmel.svg" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/md.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Macht Dampf für gutes Essen in Kita und Schule
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="banderole" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/parents.svg" /><br/>Jobs</a></div>
									<div class="banderole_el"><a href="index2.php" class="animsition-link"><img src="img/icons/kompass.svg" /><br/>So finden<br/>Sie uns</a></div>
									<div class="banderole_el"><a href="index3.php" class="animsition-link"><img src="img/icons/typo3.svg" /><br/>Typo3</a></div>
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/buch.svg" /><br/>Kontakt</a></div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="slider_wrapper">
							<div class="slider">
								<div class="swiper-container">
								    <div class="swiper-wrapper">
								        <div class="swiper-slide">
								        	<div class="swiper-slide-img"><img src="img/slide2.jpg"/></div>
								        	<div class="swiper-slide-text-wrap">
												<div class="swiper-slide-text-bg"></div>
												<div class="swiper-slide-text">TEXTTEXTETXTEXTXETXET</div>
							        		</div>
								        </div>
								        <div class="swiper-slide">
								        	<div class="swiper-slide-img"><img src="img/slide2.jpg"/></div>
								        	<div class="swiper-slide-text-wrap">
												<div class="swiper-slide-text-bg"></div>
												<div class="swiper-slide-text">TEXTTEXTETXTEXTXETXET</div>
							        		</div>
								        </div>
								        <div class="swiper-slide">
								        	<div class="swiper-slide-img"><img src="img/slide2.jpg"/></div>
								        	<div class="swiper-slide-text-wrap">
												<div class="swiper-slide-text-bg"></div>
												<div class="swiper-slide-text">TEXTTEXTETXTEXTXETXET</div>
							        		</div>
								        </div>
								        <div class="swiper-slide">
								        	<div class="swiper-slide-img"><img src="img/slide2.jpg"/></div>
								        	<div class="swiper-slide-text-wrap">
												<div class="swiper-slide-text-bg"></div>
												<div class="swiper-slide-text">TEXTTEXTETXTEXTXETXET</div>
							        		</div>
								        </div>
								        <div class="swiper-slide">
								        	<div class="swiper-slide-img"><img src="img/slide2.jpg"/></div>
								        	<div class="swiper-slide-text-wrap">
												<div class="swiper-slide-text-bg"></div>
												<div class="swiper-slide-text">TEXTTEXTETXTEXTXETXET</div>
							        		</div>
								        </div>
								    </div>
								    <div class="swiper-pagination"></div>
								</div>
							</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="banderole grey" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/parents.svg" /><br/>Jobs</a></div>
									<div class="banderole_el"><a href="index2.php" class="animsition-link"><img src="img/icons/kompass.svg" /><br/>So finden<br/>Sie uns</a></div>
									<div class="banderole_el"><a href="index3.php" class="animsition-link"><img src="img/icons/typo3.svg" /><br/>Typo3</a></div>
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/buch.svg" /><br/>Kontakt</a></div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="banderole" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/parents.svg" /><br/>Jobs</a></div>
									<div class="banderole_el"><a href="index2.php" class="animsition-link"><img src="img/icons/kompass.svg" /><br/>So finden<br/>Sie uns</a></div>
									<div class="banderole_el"><a href="index3.php" class="animsition-link"><img src="img/icons/typo3.svg" /><br/>Typo3</a></div>
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/buch.svg" /><br/>Kontakt</a></div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="banderole grey" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/parents.svg" /><br/>Jobs</a></div>
									<div class="banderole_el"><a href="index2.php" class="animsition-link"><img src="img/icons/kompass.svg" /><br/>So finden<br/>Sie uns</a></div>
									<div class="banderole_el"><a href="index3.php" class="animsition-link"><img src="img/icons/typo3.svg" /><br/>Typo3</a></div>
									<div class="banderole_el"><a href="index.php" class="animsition-link"><img src="img/icons/buch.svg" /><br/>Kontakt</a></div>
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>

	<?php include 'footer.php'; ?>