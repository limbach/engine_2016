var engine = new Object({

    menu: false,
    swiper: null,
    grid: null,
    map: null,

    initDomReady: function(){
        console.log('initDomReady');
        en.initInterdimensional();
        en.initAnimsition();
    },

    initLoad: function(){
        console.log('initLoad');
        en.initGrid();
        en.initMenu();
        en.initSwiper();
        en.initMap();
    },

    initAnimsition: function(){
        console.log('initAnimsition');
        $(".animsition").animsition({
            inClass: 'zoom-in-sm',
            outClass: 'zoom-out-sm',
            inDuration: 1500,
            outDuration: 800,
            linkElement: '.animsition-link',
            loading: true,
            loadingParentElement: 'body',
            loadingClass: 'animsition-loading',
            loadingInner: '',
            timeout: false,
            timeoutCountdown: 5000,
            onLoadEvent: true,
            browser: [ 'animation-duration', '-webkit-animation-duration'],
            overlay : false,
            overlayClass : 'animsition-overlay-slide',
            overlayParentElement : 'body',
            transition: function(url){ window.location.href = url; }
        }); 
    },

    initGrid: function(){
        console.log('initGrid');
        en.grid = $('.grid').masonry({
            columnWidth: '.grid-sizer',
            itemSelector: '.grid-item',
            percentPosition: true,
            gutter: 0
        });
    },

    initInterdimensional: function(){
        console.log('initInterdimensional');
        Interdimensional.charge({
            PPD: 0.8,
            insensitivity: 5,
            useControl: true,
            control: null  
        });
    },

    initMenu: function(){
        console.log('initMenu');


        $('.menu_toggle').unbind('click').click(function(){
            console.log('burgerz'+ Math.round(Math.random()*100));
            $('.hamburger').toggleClass("is-active");
            $('#perspective_overlay').fadeToggle(1000);
            $('#perspective').toggleClass('closed');
            $('#perspective_overlay').css({
                'height': $('main').height() + $('footer').height() + 100
            });
            $('.menu_wrap').toggleClass('act');
        });

        $('#perspective_overlay').unbind('click').click(function(event){
            $('.menu_toggle').trigger('click');
        });

        $('.menu a').each(function(){
            var ma = $(this);
            ma.unbind('click').click(function(e){
                e.preventDefault();
                $('.menu a').removeClass('act');
                ma.addClass('act');
                var dest = ma.attr('href')+' #side_wrapper > *';
                console.log(dest);
                $('#side_wrapper').fadeOut('fast',function(){
                    en.destroyAll();
                    $('#side_wrapper').find('main').remove();
                    $('#side_wrapper').find('footer').remove();
                    $('#side_wrapper').load(dest,function(){
                        $('#side_wrapper').fadeIn('fast',function(){
                            $('.menu_toggle').trigger('click');
                            en.initSwiper();
                            en.initGrid();
                        });
                    });
                });
            });
        });
    },

    initMidnight: function(){
        console.log('initMidnight');
        $('.header_wrap').midnight();
    },

    initSwiper: function(){
        console.log('initSwiper');
        en.swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            autoplay: 2500,
            speed: 2500,
            loop: true,
            onSlideChangeStart:function(){

            },
            onSlideChangeEnd:function(){

            }
        });
    },

    initMap: function(){
        // AIzaSyBO0O0PRBgrosQn0WfLrx8kg2V7NNDD_Gg
        console.log('initMap')
        if($('#map').size()>0){

            var customMapType = new google.maps.StyledMapType([
                {
                    stylers: [
                        {hue: '#ececec'},
                        {visibility: 'simplified'},
                        {gamma: 0.5},
                        {weight: 0.5}
                    ]
                },
                  // {
                  //   elementType: 'labels',
                  //   stylers: [{"visibility": 'off'}]
                  // },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                    { "color": "#000000" }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                    { "color": "#ee00ee" }
                    ]
                },
                {
                    "featureType": "landscape",
                    "stylers": [
                    { "color": "#FFA500" }
                    ]
                },
                {
                    "featureType": "poi",
                    "stylers": [
                    { "color": "#FFA500 " }
                    ]
                },
                {
                    "featureType": "transit",
                    "stylers": [
                        { "color": "#b37400" }
                    ]
                },
            ],
            {
                name: 'engine-productions GmbH'
            });

            var eplatlng = {lat: 50.9343802, lng: 6.9372008};


            en.map = new google.maps.Map( document.getElementById('map'), {
                zoom: 13,
                center: eplatlng,
                disableDefaultUI: true,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
                }
            });

            var marker = new google.maps.Marker({
                position: eplatlng,
                map: en.map
                // ,title: 'Click to zoom'
            });
            marker.addListener('click', function(){
                en.map.setZoom(16);
                en.map.setCenter(marker.getPosition());
                toggleBounce();
            });
            function toggleBounce() {
              if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
              } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
              }
            }

            // map.addListener('center_changed', function() {
            //     // 3 seconds after the center of the map has changed, pan back to the
            //     // marker.
            //     window.setTimeout(function() {
            //         map.panTo(marker.getPosition());
            //     }, 3000);
            // });

            // marker.addListener('click', function() {
            //     map.setZoom(16);
            //     map.setCenter(marker.getPosition());
            // });

            var customMapTypeId = 'custom_style';


            en.map.mapTypes.set(customMapTypeId, customMapType);
            en.map.setMapTypeId(customMapTypeId);

      }
  },

  destroyAll:function(){
    console.log('destroyAll')
    console.log(en.swiper!=null);
    console.log(en.grid!=null);
    if(typeof en.swiper.destroy === 'function'){
        en.swiper.destroy();
        en.swiper = null;
    }
    if(en.grid != null){
        en.grid.masonry('destroy');
        en.grid = null;
    }
}

});

var en = engine;

$(document).ready(function () {
    en.initDomReady();
});
$(window).resize(function () {

});
$(window).load(function () {
    en.initLoad();
});