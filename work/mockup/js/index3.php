	<?php include 'header.php'; ?>

					<section>
						<div class="grid">
							<div class="grid-sizer"></div>
							<div class="grid-item zwei_eins">
								<div class="grid-item-logo">
									<img src="img/logos/amazon.jpg" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/tw.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Lorem Ipsum dolor sit amet
								</div>
							</div>
							<div class="grid-item">
								<div class="grid-item-logo">
									<img src="img/logos/amazon.jpg" />
								</div>
								<div class="grid-item-image">
									<a href="index.php" class="animsition-link"><img src="img/projekte/tw.jpg" /></a>
								</div>
								<div class="grid-item-text">
									Lorem Ipsum dolor sit amet
								</div>
							</div>
							<div class="grid-item zwei_eins">
								<a href="index2.php" class="animsition-link"><img src="img/projekte/vdk.jpg" /></a>
							</div>
							<div class="grid-item zwei_zwei">
								<a href="index3.php" class="animsition-link"><img src="img/projekte/mgh.jpg" /></a>
							</div>
							<div class="grid-item eins_zwei">
								<a href="index.php" class="animsition-link"><img src="img/projekte/lw.jpg" /></a>
							</div>
							<div class="grid-item eins_zwei">
								<a href="index2.php" class="animsition-link"><img src="img/projekte/lw.jpg" /></a>
							</div>
							<div class="grid-item zwei_eins">
								<a href="index3.php" class="animsition-link"><img src="img/projekte/amball.jpg" /></a>
							</div>
							<div class="grid-item zwei_eins">
								<a href="index2.php" class="animsition-link"><img src="img/projekte/bravo.jpg" /></a>
							</div>
							<div class="grid-item zwei_eins">
								<a href="index.php" class="animsition-link"><img src="img/projekte/amball.jpg" /></a>
							</div>
							<div class="grid-item zwei_zwei">
								<a href="index2.php" class="animsition-link"><img src="img/projekte/105.jpg" /></a>
							</div>
							<div class="grid-item zwei_eins">
								<a href="index3.php" class="animsition-link"><img src="img/projekte/bravo.jpg" /></a>
							</div>
							<div class="grid-item zwei_zwei">
								<a href="index.php" class="animsition-link"><img src="img/projekte/md.jpg" /></a>
							</div>
							<div class="grid-item eins_zwei">
								<a href="index2.php" class="animsition-link"><img src="img/projekte/lw.jpg" /></a>
							</div>
							<div class="grid-item eins_zwei">
								<a href="index3.php" class="animsition-link"><img src="img/projekte/lw.jpg" /></a>
							</div>
						</div>
					</section>
					<section>
						<div class="banderole" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									Jobs
									So finden Sie uns
									Typo3
									Kontakt
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="slider_wrapper">
							<div class="slider">
								<div class="swiper-container">
								    <div class="swiper-wrapper">
								        <div class="swiper-slide"><img src="img/slide2.jpg"/></div>
								        <div class="swiper-slide"><img src="img/slide2.jpg"/></div>
								        <div class="swiper-slide"><img src="img/slide2.jpg"/></div>
								        <div class="swiper-slide">
								        	<div style="margin:auto;width: 80px;">Slide 1</div>
							        	</div>
								        <div class="swiper-slide"><img src="img/slide2.jpg"/></div>
								        <div class="swiper-slide">Slide 2</div>
								        <div class="swiper-slide"><img src="img/slide.jpg"/></div>
								        <div class="swiper-slide">Slide 3</div>
								    </div>
								    <div class="swiper-pagination"></div>
								</div>
							</div>
					</section>
					<section>
						<div class="banderole grey" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									<div class="banderole_content_el">Jobs</div>
									<div class="banderole_content_el">So finden Sie uns</div>
									<div class="banderole_content_el">Typo3</div>
									<div class="banderole_content_el">Kontakt</div>

								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="banderole" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									Jobs
									So finden Sie uns
									Typo3
									Kontakt
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="banderole grey" data-midnight="invert">
							<div class="banderole_bg"></div>
							<div class="banderole_content_wrap">
								<div class="banderole_content">
									Jobs
									So finden Sie uns
									Typo3
									Kontakt
								</div>
							</div>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>
					<section>
						<div class="text">
							<h1>Herzlich willkommen bei den engines!</h1>
							<p><b>Seit 1998 ist engine-productions Ihr Partner für Medienproduktionen aller Art.</b></p>
							<p>Unser Kerngeschäft umfasst internet- und intranetbasierte Softwarelösungen, Point-Of-Sale-, E-Commerce- sowie Content Management Systeme und wird durch umfangreiche Beratungsleistung sowie Medienservices ergänzt.</p>
							<p>Mit unseren Partnern decken wir gemeinsam alle relevanten Dienstleistungsbereiche ab, um Ihr Projekt erfolgreich zu planen, umzusetzen und nachhaltig zu betreuen.</p>
							<p>Durch den Einsatz aktueller Technologien stellen wir die Verfügbarkeit Ihrer Präsenz auf allen gängigen und kommenden Endgeräten sicher und transportieren Ihre Inhalte effizient an Ihre Zielgruppe.</p>
							<p>Als technischer Dienstleister unterstützen wir auch Agenturen bei Großprojekten mit unserer langjährigen Erfahrung in webbasierter Softwareentwicklung und der Individualisierung von Frameworks und Open Source-Systemen.</p>
							<p>Machen Sie sich mit unseren Referenzen vertraut oder verschaffen Sie sich einen Überblick über unsere Leistungen!</p>
						</div>
					</section>

	<?php include 'footer.php'; ?>